
import 'package:flutter/material.dart';

import '../../data/photo_list.dart';
import '../../data/repository.dart';


class PhotoController extends ChangeNotifier {
  final List<Photo> _photos = [];
  bool _isLoading = false;

  List<Photo> get photos => _photos;
  bool get isLoading => _isLoading;

  setLoading(bool loading) {
    _isLoading = loading;
    notifyListeners();
  }

  fetchData({int page=1}) async {
    photos.clear();
    setLoading(true);
    final photoRepository = PhotoRepository();
    photos.addAll(await photoRepository.photos(page: page));
    setLoading(false);
    notifyListeners();
  }

  void add(Photo image) {
    _photos.add(image);
    notifyListeners();
  }

  void remove(Photo image) {
    _photos.remove(image);
    notifyListeners();
  }

  bool isMarked(Photo image) {
    return _photos.contains(image);
  }
  
  void removeAll() {
    _photos.clear();
    notifyListeners();
  }
}
